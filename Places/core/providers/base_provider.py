from abc import ABC, abstractmethod
from core.parsers.json_parser import JSONParser


class BaseProvider(ABC):

    def __init__(self):
        self.setup()

    @abstractmethod
    def setup(self):
        self.url = ""
        self.detail_url = ""
        self.response_parser = JSONParser()

    @abstractmethod
    def perform_search(self, keyword, lat, long, page_size=20, request=None, **kwargs):
        pass

    @abstractmethod
    def prepare_response(self, response):
        pass