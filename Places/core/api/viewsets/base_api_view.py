from collections import OrderedDict
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination


class BaseAPIListView(APIView, PageNumberPagination):

    def get_queryset(self, request):
        return []

    def get_paginated_queryset(self, request):
        queryset = self.get_queryset(request)
        return self.paginate_queryset(queryset, request)

    def get(self, request, **kwargs):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', settings.REST_FRAMEWORK.get("PAGE_SIZE", 1))
        objects = self.get_paginated_queryset(request)
        return self.get_paginated_response(objects, page, page_size)

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', None),
            ('previous', None),
            ('page_size', page_num),
            ('results', data)
        ]))