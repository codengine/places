import json

from core.parsers.base_parser import BaseParser


class JSONParser(BaseParser):

    def parse(self, content):
        try:
            content = str(content.replace(b'\n', b''))
            print(content)
            return json.loads(content)
        except Exception as exp:
            pass
