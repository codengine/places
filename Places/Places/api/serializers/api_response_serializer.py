from rest_framework import serializers


class APIResponseSerializer(serializers.Serializer):
    ID = serializers.CharField(max_length=500, allow_blank=True)
    Provider = serializers.CharField(max_length=500, allow_blank=True)
    Name = serializers.CharField(max_length=500, allow_blank=True)
    Description = serializers.CharField(max_length=500, allow_blank=True)
    Location = serializers.CharField(max_length=500, allow_blank=True)
    Address = serializers.CharField(max_length=500, allow_blank=True)
    URL = serializers.CharField(max_length=500, allow_blank=True)