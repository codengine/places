from Places.api.serializers.api_response_serializer import APIResponseSerializer
from Places.decorators.enablers import enable_providers
from Places.handlers.places_api_handler import PlacesAPIHandler
from core.api.viewsets.base_api_view import BaseAPIListView
from django.conf import settings


class PlacesSearchAPIView(BaseAPIListView):
    http_method_names = ["get"]

    @enable_providers
    def get_queryset(self, request):
        keyword = request.GET.get("keyword")
        location = request.GET.get("location")
        radius = request.GET.get("radius")
        page_size = settings.PLACES_PAGE_SIZE
        search_results = PlacesAPIHandler.handle_search(providers=self.providers, keyword=keyword,
                                                        location=location, radius=radius, page_size=page_size, request=request)

        print(search_results)
        search_result = APIResponseSerializer(search_results, many=True)

        return search_result.data