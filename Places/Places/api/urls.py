from Places.api.viewsets.places_search_viewsets import PlacesSearchAPIView
from django.conf.urls import url

urlpatterns = [
    url(r'^search-places/$', PlacesSearchAPIView.as_view(), name='search_places'),
]