from django.conf import settings
from Places.parsers.response_parser import APIResponseParser
from Places.providers.places_provider import PlacesProvider


class GooglePlacesProvider(PlacesProvider):

    def __str__(self):
        return "Google Places"

    def skip_detail_search(self):
        return False

    def setup(self):
        self.url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
        self.detail_url = "https://maps.googleapis.com/maps/api/place/details/json"
        self.response_parser = APIResponseParser()

    def prepare_places_request_url(self, base_url, keyword, location, radius, page_size=20, request=None, **kwargs):
        google_places_api_key = settings.GOOGLE_PLACES_API_KEY
        # return base_url+"?query=123+main+street&location=42.3675294,-71.186966&key=%s" % google_places_api_key
        url = base_url + "?query=%s" % keyword
        if location and radius:
            url += "&location=%s&radius=%s" % (location, radius)
        url += "&key=%s" % google_places_api_key
        return url

    def prepare_details_place_url(self, place_id):
        google_places_api_key = settings.GOOGLE_PLACES_API_KEY
        url = self.detail_url + "?placeid=%s&key=%s" % (place_id, google_places_api_key)
        return url

    def prepare_response(self, response):
        """
        ID
        Provider
        Name
        Description
        Location (lat, lng) (if applicable)
        Address (if applicable)
        URI of the place where more details are available
        """

        def get_location(result):
            try:
                return "%s, %s" % (result["geometry"]['location']['lat'], result["geometry"]['location']['lng'])
            except Exception as exp:
                pass

        formatted_response = []
        if response['status'] == 'OK':
            if response['results']:
                for result in response['results']:
                    location = get_location(result)
                    formatted_result = {
                            'Provider': self.__str__(),
                            'ProviderInstance': self,
                            'ID': result.get('place_id'),
                            'Name': result.get('name'),
                            'Address': result.get('formatted_address'),
                        }
                    if location:
                        formatted_result['Location'] = location
                    formatted_response += [
                        formatted_result
                    ]
        return formatted_response

    def prepare_detail_response(self, place_detail_response):
        response = {}
        if place_detail_response['status'] == 'OK' and type(place_detail_response['result']) is dict:
            response['URL'] = place_detail_response['result'].get('website')
            response['Description'] = None
        return response