from Places.handlers.http_handler import HttpRequestHandler
from abc import abstractmethod
from core.providers.base_provider import BaseProvider


class PlacesProvider(BaseProvider):

    def skip_detail_search(self):
        return True

    @abstractmethod
    def prepare_detail_response(self, place_detail_response):
        pass

    @abstractmethod
    def prepare_places_request_url(self, base_url, keyword, location, radius, page_size=20, request=None, **kwargs):
        pass

    @abstractmethod
    def prepare_details_place_url(self, place_id):
        pass

    def perform_search(self, keyword, location, radius, page_size=20, request=None, **kwargs):
        try:
            request_url = self.prepare_places_request_url(base_url=self.url, keyword=keyword,
                                                          location=location, radius=radius, page_size=page_size, **kwargs)
            content = HttpRequestHandler.perform_api_request(request_url, **kwargs)
            # parsed_response = self.response_parser.parse(content=content) if content else {}
            # print(parsed_response)
            formatted_response = self.prepare_response(content)
            return formatted_response
        except Exception as exp:
            print(str(exp))

    def perform_detail_search(self, place_id, basic_info={}):
        try:
            if self.skip_detail_search():
                return basic_info
            request_url = self.prepare_details_place_url(place_id=place_id)
            content = HttpRequestHandler.perform_api_request(request_url)
            place_detail_response = self.prepare_detail_response(content) or {}
            # Merge basic info and place detail response
            return {**basic_info, **place_detail_response}
        except Exception as exp:
            pass
