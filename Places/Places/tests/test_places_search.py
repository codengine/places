from Places.decorators.enablers import enable_providers
from Places.providers.places_provider import PlacesProvider
from django.test import TestCase
from rest_framework.test import APIClient


class TestPlacesAPI(TestCase):

    def setUp(self):
        print("Setting up...")

    def test_api_risk_create(self):
        client = APIClient()
        response = client.get('http://127.0.0.1:8002/api/v1/search-places/?keyword=Dhaka', format='json')
        self.assertEqual(response.status_code, 200)

    def test_provider_loader(self):
        class TestProvider(object):

            @enable_providers
            def method(self):
                pass

        x = TestProvider()
        x.method()
        self.assertTrue(getattr(x, 'providers'))

    def test_providers(self):
        class TestProvider(object):
            @enable_providers
            def method(self):
                pass

        x = TestProvider()
        x.method()
        for provider in getattr(x, 'providers'):
            self.assertTrue(issubclass(provider, PlacesProvider))

    def tearDown(self):
        print("Cleared...")
