from pydoc import locate


def load_module(module_path):
    try:
        return locate(module_path)
    except Exception as exp:
        pass