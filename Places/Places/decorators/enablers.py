from Places.loaders.loaders import load_module
from functools import wraps
from django.conf import settings


def enable_providers(func):

    @wraps(func)
    def embed_providers(*args, **kwargs):
        places_providers = getattr(settings, 'PLACES_PROVIDERS') or []
        import_module = lambda module_path: load_module(module_path)
        places_providers = [import_module(provider) for provider in places_providers]
        if args:
            setattr(args[0], 'providers', places_providers)
        response = func(*args, **kwargs)
        return response
    return embed_providers