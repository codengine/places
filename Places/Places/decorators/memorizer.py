from Places.handlers.redis_cache_handler import RedisCacheHandler
from Places.loaders.loaders import load_module
from functools import wraps
import json
from django.conf import settings
import copy

__all__ = ['memorizer']


def memorizer(context):
    def mem_cache(func):
        cache = RedisCacheHandler

        def find_matched_provider(provider_name, provider_list):
            for provider in provider_list:
                if provider_name in provider:
                    return provider

        async def call_original(f, *args, **kwargs):
            response = await f(*args, **kwargs)
            for item in response:
                if 'ProviderInstance' in item:
                    pinstance = item['ProviderInstance']
                    del item['ProviderInstance']
                    item['ProviderClass'] = pinstance.__class__.__name__
            print(response)
            response = json.dumps(response)
            return response

        @wraps(func)
        async def wrapper(*args, **kwargs):
            key = kwargs
            if 'request' in key:
                del key['request']

            cached_value = cache.read_cache(key)
            if not cached_value:
                print('Not Found!')
                response = await func(*args, **kwargs)
                return_copy = copy.deepcopy(response)
                if context == 'places':
                    for item in response:
                        if 'ProviderInstance' in item:
                            pinstance = item['ProviderInstance']
                            del item['ProviderInstance']
                            item['ProviderClass'] = pinstance.__class__.__name__
                # print(response)
                response = json.dumps(response)
                cache.update_cache(key, response)
                return return_copy
            else:
                print('Found!')
                print(cached_value)
                cached_value = cached_value.decode('utf-8')
                cached_value = json.loads(cached_value, encoding='utf-8')
                missed_any = False
                if context == 'places':
                    new_cache = []
                    for item in cached_value:
                        if 'ProviderClass' in item:
                            pclass = item['ProviderClass']
                            del item['ProviderClass']
                            provider_full_path = find_matched_provider(pclass, settings.PLACES_PROVIDERS)
                            if provider_full_path:
                                provider_class = load_module(provider_full_path)
                                if not provider_class:
                                    missed_any = True
                                else:
                                    item['ProviderInstance'] = provider_class()
                            else:
                                missed_any = True
                        new_cache += [item]
                    if missed_any:
                        response = await func(*args, **kwargs)
                        for item in response:
                            if 'ProviderInstance' in item:
                                pinstance = item['ProviderInstance']
                                del item['ProviderInstance']
                                item['ProviderClass'] = pinstance.__class__.__name__
                        response = json.dumps(response)
                        cache.update_cache(key, response)
                        return response
                    else:
                        return new_cache
                else:
                    return cached_value
        return wrapper
    return mem_cache