import requests


class HttpRequestHandler(object):

    @classmethod
    def perform_api_request(cls, url, **kwargs):
        try:
            response = requests.get(url=url).json()
            return response
        except Exception as exp:
            pass