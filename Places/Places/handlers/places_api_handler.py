import asyncio
from Places.decorators.memorizer import memorizer
from functools import lru_cache


class PlacesAPIHandler(object):

    @classmethod
    def handle_search_places(cls, providers, **kwargs):
        loop = asyncio.get_event_loop()

        @memorizer(context='places')
        async def search_places(providers, **kwargs):
            search_ = lambda provider: provider().perform_search(**kwargs)
            results = []
            futures = [loop.run_in_executor(None, search_, provider) for provider in providers if provider]
            for response in await asyncio.gather(*futures):
                if response:
                    results += response if type(response) is list else [response]
            return results

        places = loop.run_until_complete(search_places(providers=providers, **kwargs))
        return places

    @classmethod
    def handle_details_search(cls, places, **kwargs):
        loop = asyncio.get_event_loop()

        @memorizer(context='details')
        async def search_place_details(places, **kwargs):
            place_details = []

            def exclude_provider_instance(place):
                del place["ProviderInstance"]
                return place

            filtered_places = list(filter(lambda p: 'ProviderInstance' in p and 'ID' in p, places))
            if len(filtered_places) != len(places):
                print("Warning! The output contains no provider instance and place id")
            futures = [loop.run_in_executor(None, place['ProviderInstance'].perform_detail_search, place['ID'],
                                            exclude_provider_instance(place)) for place in places if filtered_places]
            for response in await asyncio.gather(*futures):
                if response:
                    place_details += response if type(response) is list else [response]
            return place_details

        results = loop.run_until_complete(search_place_details(places=places, **kwargs))
        return results

    @classmethod
    def handle_search(cls, providers=[], **kwargs):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        places = cls.handle_search_places(providers=providers, **kwargs)
        details = cls.handle_details_search(places=places, **kwargs)
        loop.close()
        return details