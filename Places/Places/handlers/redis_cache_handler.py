import redis
from django.conf import settings


class RedisCacheHandler(object):
    """
    It can be implemented as LRU cache as required.
    """
    @classmethod
    def update_cache(cls, key, value):
        redis_connection = settings.REDIS_CONNECTION
        redis_connection.set(key, value)

    @classmethod
    def read_cache(cls, key):
        redis_connection = settings.REDIS_CONNECTION
        # redis_connection.delete(key)
        return redis_connection.get(key)